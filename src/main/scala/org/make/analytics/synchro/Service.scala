package org.make.analytics.synchro

import java.nio.file.{Files, Path}
import java.time.{LocalDate, ZonedDateTime}
import com.github.tototoshi.csv.{CSVWriter, DefaultCSVFormat, QUOTE_MINIMAL, Quoting}
import org.make.analytics.synchro.client.{ApiClient, DialClient, FacebookClient}
import org.make.analytics.synchro.client.FacebookClient.{DailyHeaders, Insights}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Using

object Service {

  private def writeInsights(path: Path, insights: Seq[Insights]): Unit = {
    implicit object MyFormat extends DefaultCSVFormat {
      override val quoting: Quoting = QUOTE_MINIMAL
    }

    Using(CSVWriter.open(path.toFile)) { writer =>
      writer.writeRow(DailyHeaders.headers.map(_.header))
      insights.foreach { insight => writer.writeRow(DailyHeaders.headers.map(_.value(insight))) }
    }
  }

  def dailyReport()(
    implicit executionContext: ExecutionContext,
    facebookClient: FacebookClient,
    apiClient: ApiClient,
    dialClient: DialClient
  ): Future[Unit] = {
    val futureInsights = for {
      slugs    <- apiClient.listActiveQuestionSlugs(ZonedDateTime.now().minusDays(1))
      insights <- getInsights(slugs)
    } yield insights

    futureInsights.flatMap { insights =>
      if (insights.values.flatten.isEmpty) {
        println("No insights to send to the dial")
        Future.unit
      } else {
        val path = createCsvs(insights)
        dialClient.sendReport(path)
      }
    }
  }

  def createCsvs(
    insightsBySlug: Map[String, Seq[Insights]]
  )(implicit executionContext: ExecutionContext, facebookClient: FacebookClient): Path = {
    val date = LocalDate.now().minusDays(1).toString
    val directory = Files.createTempDirectory(date)
    directory.toFile.deleteOnExit()
    val path = Files.createFile(directory.resolve(s"Suivi-quotidien-$date.csv"))
    path.toFile.deleteOnExit()
    val insights = insightsBySlug.values.flatten
    writeInsights(path, insights.toSeq)
    path
  }

  private def getInsights(
    questionSlugs: Seq[String]
  )(implicit executionContext: ExecutionContext, facebookClient: FacebookClient): Future[Map[String, Seq[Insights]]] = {
    facebookClient
      .questionCampaigns()
      .flatMap { questionsCampaigns =>
        Future.traverse(questionsCampaigns.filter(qc => questionSlugs.contains(qc.questionSlug))) { questionCampaigns =>
          facebookClient
            .insightsForQuestionCampaign(questionCampaigns)
            .map(questionCampaigns.questionSlug -> _)
        }
      }
      .map(_.toMap)
  }
}
