package org.make.analytics.synchro

import akka.actor.ActorSystem
import org.make.analytics.synchro.client.{ApiClient, DialClient, FacebookClient}

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

object Main extends App {

  implicit val actorSystem: ActorSystem = ActorSystem("analytics-synchro")

  implicit private val apiClient: ApiClient = new ApiClient()
  implicit private val facebookClient: FacebookClient = new FacebookClient()
  implicit private val dialClient: DialClient = new DialClient()

  try {
    Await.result(Service.dailyReport(), atMost = 5.minutes)
    actorSystem.terminate()
    System.exit(0)
  } catch {
    case e: Throwable =>
      actorSystem.terminate()
      e.printStackTrace()
      System.exit(1)
  }
}
