package org.make.analytics.synchro.client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.Accept
import akka.http.scaladsl.unmarshalling.PredefinedFromEntityUnmarshallers.stringUnmarshaller
import akka.http.scaladsl.unmarshalling.Unmarshal
import com.typesafe.config.Config
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport
import io.circe.Decoder
import org.make.analytics.synchro.client.FacebookClient.{
  Alias,
  FacebookResponse,
  Insights,
  InsightsResponse,
  NameAndId,
  QuestionCampaigns
}

import java.time.LocalDate
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.jdk.CollectionConverters._

class FacebookClient(implicit val actorSystem: ActorSystem) extends ErrorAccumulatingCirceSupport {

  private val config: Config = actorSystem.settings.config.getConfig("analytics-synchro")
  private val facebookApiVersion = "v12.0"
  private val baseUrl = s"https://graph.facebook.com/$facebookApiVersion"

  private val aliases: Seq[Alias] = config.getObject("facebook").keySet.asScala.toSeq.map { aliasName =>
    val accessToken: String = config.getString(s"facebook.$aliasName.token")
    val accountId: String = config.getString(s"facebook.$aliasName.account-id")
    Alias(aliasName, accessToken, accountId)
  }

  private val SlugExtractor = ".*\\[utm_campaign=([^]]+)]".r

  def questionCampaigns(): Future[Seq[QuestionCampaigns]] = {
    Future
      .traverse(aliases) { alias => campaigns(alias.accountId, alias.accessToken).map(alias -> _) }
      .map(_.flatMap {
        case (alias, nameAndIds) =>
          nameAndIds.collect {
            case NameAndId(id, SlugExtractor(name)) => NameAndId(id, name)
          }.groupMap(_.name)(_.id).map {
            case (questionSlug, campaignIds) => QuestionCampaigns(questionSlug, campaignIds, alias)
          }
      })
  }

  def campaigns(accountId: String, token: String): Future[Seq[NameAndId]] = {
    val url: String = s"$baseUrl/act_$accountId/campaigns?access_token=$token&fields=id,name&date_preset=yesterday"
    callFacebook[NameAndId](url)
  }

  def insightsForQuestionCampaign(questionCampaigns: QuestionCampaigns): Future[Seq[Insights]] = {
    Future
      .traverse(questionCampaigns.campaignIds) { campaignId =>
        insights(campaignId, questionCampaigns.alias.accessToken)
          .map(_.map(_.toInsights(questionCampaigns.alias.name)))
      }
      .map(_.flatten)
  }

  def insights(campaignId: String, token: String): Future[Seq[InsightsResponse]] = {
    val url: String = s"$baseUrl/$campaignId/insights?access_token=$token" +
      "&fields=campaign_name,campaign_id,adset_name,ad_name,ad_id,spend,reach,frequency,outbound_clicks,cost_per_outbound_click,outbound_clicks_ctr,impressions" +
      "&level=ad&date_preset=yesterday"
    callFacebook[InsightsResponse](url)
  }

  private def callFacebook[T](url: String)(implicit decoder: Decoder[T]): Future[Seq[T]] = {
    singleCallFacebook[T](url).flatMap(getAllData[T]).map(_.data)
  }

  private def singleCallFacebook[T](url: String)(implicit decoder: Decoder[T]): Future[FacebookResponse[T]] = {
    Http().singleRequest(HttpRequest(uri = Uri(url), headers = Seq(Accept(MediaTypes.`application/json`)))).flatMap {
      case HttpResponse(StatusCodes.OK, _, entity, _) =>
        Unmarshal(entity).to[FacebookResponse[T]]
      case response =>
        Unmarshal(response.entity).to[String].flatMap { entity =>
          Future.failed(new IllegalStateException(s"$response -> $entity"))
        }
    }
  }

  // Recursively paginate in order to retrieve all the available data
  // If there is too much data, an OutOfMemoryError may be thrown.
  // The Xms and Xmx parameters will have to be adjusted accordingly
  private def getAllData[T](
    accumulator: FacebookResponse[T]
  )(implicit decoder: Decoder[T]): Future[FacebookResponse[T]] = {
    accumulator.paging.flatMap(_.next) match {
      case None => Future.successful(accumulator)
      case Some(next) =>
        singleCallFacebook(next).flatMap { data =>
          val accumulated = accumulator.copy(
            paging = accumulator.paging.map(_.copy(next = data.paging.flatMap(_.next))),
            data = accumulator.data ++ data.data
          )
          getAllData(accumulated)
        }
    }
  }

}

object FacebookClient {
  case class NameAndId(id: String, name: String)

  object NameAndId {
    implicit val decoder: Decoder[NameAndId] = io.circe.generic.semiauto.deriveDecoder
  }

  case class FacebookPaging(next: Option[String], previous: Option[String])

  object FacebookPaging {
    implicit val decoder: Decoder[FacebookPaging] = io.circe.generic.semiauto.deriveDecoder
  }

  case class FacebookResponse[T](data: Seq[T], paging: Option[FacebookPaging])

  object FacebookResponse {
    implicit def decoder[T](implicit d: Decoder[T]): Decoder[FacebookResponse[T]] =
      io.circe.generic.semiauto.deriveDecoder
  }

  case class InsightFromAction(actionType: String, value: String)

  object InsightFromAction {
    implicit val decoder: Decoder[InsightFromAction] =
      Decoder.forProduct2("action_type", "value")(InsightFromAction.apply)
  }

  final case class InsightsResponse(
    campaignName: String,
    campaignId: String,
    adsetName: String,
    adName: String,
    addId: String,
    spend: Double,
    reach: Int,
    frequency: Double,
    outboundClicks: Option[Seq[InsightFromAction]],
    costPerOutboundClick: Option[Seq[InsightFromAction]],
    outboundClicksCtr: Option[Seq[InsightFromAction]],
    startDate: Option[LocalDate],
    endDate: Option[LocalDate],
    impressions: Int
  ) {
    def toInsights(aliasName: String): Insights = Insights(
      campaignName = campaignName,
      campaignId = campaignId,
      adsetName = adsetName,
      adName = adName,
      addId = addId,
      spend = spend,
      reach = reach,
      frequency = frequency,
      outboundClicks = outboundClicks,
      costPerOutboundClick = costPerOutboundClick,
      outboundClicksCtr = outboundClicksCtr,
      startDate = startDate,
      endDate = endDate,
      impressions = impressions,
      aliasName = aliasName
    )
  }

  object InsightsResponse {
    implicit val decoder: Decoder[InsightsResponse] = Decoder.forProduct14(
      "campaign_name",
      "campaign_id",
      "adset_name",
      "ad_name",
      "ad_id",
      "spend",
      "reach",
      "frequency",
      "outbound_clicks",
      "cost_per_outbound_click",
      "outbound_clicks_ctr",
      "start_date",
      "end_date",
      "impressions"
    )(InsightsResponse.apply)
  }

  final case class Insights(
    campaignName: String,
    campaignId: String,
    adsetName: String,
    adName: String,
    addId: String,
    spend: Double,
    reach: Int,
    frequency: Double,
    outboundClicks: Option[Seq[InsightFromAction]],
    costPerOutboundClick: Option[Seq[InsightFromAction]],
    outboundClicksCtr: Option[Seq[InsightFromAction]],
    startDate: Option[LocalDate],
    endDate: Option[LocalDate],
    impressions: Int,
    aliasName: String
  )

  final case class QuestionCampaigns(questionSlug: String, campaignIds: Seq[String], alias: Alias)

  sealed trait DailyHeaders {
    def header: String
    def value(insight: Insights): String
  }

  object DailyHeaders {
    case object CampaignName extends DailyHeaders {
      override val header: String = "Nom de la campagne"
      override def value(insight: Insights): String = insight.campaignName
    }

    case object AdSetName extends DailyHeaders {
      override val header: String = "Nom de l’ensemble de publicités"
      override def value(insight: Insights): String = insight.adsetName
    }

    case object AdName extends DailyHeaders {
      override val header: String = "Nom de la publicité"
      override def value(insight: Insights): String = insight.adName
    }

    case object DeliveryStatus extends DailyHeaders {
      override val header: String = "Statut de diffusion"
      override def value(insight: Insights): String = "unknown"
    }

    case object DeliveryLevel extends DailyHeaders {
      override val header: String = "Niveau de diffusion"
      override def value(insight: Insights): String = "ad"
    }

    case object SpentAmount extends DailyHeaders {
      override val header: String = "Montant dépensé (EUR)"
      override def value(insight: Insights): String = insight.spend.toString
    }

    case object Coverage extends DailyHeaders {
      override val header: String = "Couverture"
      override def value(insight: Insights): String = insight.reach.toString
    }

    case object Frequency extends DailyHeaders {
      override val header: String = "Répétition"
      override def value(insight: Insights): String = insight.frequency.toString
    }

    case object Clicks extends DailyHeaders {
      override val header: String = "Clics sur un lien"
      override def value(insight: Insights): String = insight.outboundClicks.map(_.head.value).getOrElse("0")
    }

    case object CPC extends DailyHeaders {
      override val header: String = "CPC (coût par clic sur un lien)"
      override def value(insight: Insights): String = insight.costPerOutboundClick.map(_.head.value).getOrElse("0")
    }

    case object CTR extends DailyHeaders {
      override val header: String = "CTR (taux de clics sur le lien)"
      override def value(insight: Insights): String = insight.outboundClicksCtr.map(_.head.value).getOrElse("0")
    }

    case object StartDate extends DailyHeaders {
      override val header: String = "Début des rapports"
      override def value(insight: Insights): String = LocalDate.now().minusDays(1).toString
    }

    case object EndDate extends DailyHeaders {
      override val header: String = "Fin des rapports"
      override def value(insight: Insights): String = LocalDate.now().minusDays(1).toString
    }

    case object Impressions extends DailyHeaders {
      override val header: String = "Impressions"
      override def value(insight: Insights): String = insight.impressions.toString
    }

    case object AliasName extends DailyHeaders {
      override val header: String = "Alias de compte facebook"
      override def value(insight: Insights): String = insight.aliasName
    }

    val headers: Seq[DailyHeaders] = Seq(
      CampaignName,
      AdSetName,
      AdName,
      DeliveryStatus,
      DeliveryLevel,
      SpentAmount,
      Coverage,
      Frequency,
      Clicks,
      CPC,
      CTR,
      StartDate,
      EndDate,
      Impressions,
      AliasName
    )
  }

  final case class Alias(name: String, accessToken: String, accountId: String)
}
