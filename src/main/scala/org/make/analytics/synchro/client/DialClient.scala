package org.make.analytics.synchro.client

import java.nio.file.Path

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.PredefinedFromEntityUnmarshallers.stringUnmarshaller
import akka.http.scaladsl.unmarshalling.Unmarshal

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.Source
import scala.util.Using

class DialClient(implicit val actorSystem: ActorSystem) {
  private val config =
    actorSystem.settings.config.getConfig("analytics-synchro.dial")

  private val url: String = config.getString("url")
  private val apiKey = config.getString("dial-secure-key")

  def sendReport(csv: Path): Future[Unit] = {
    val data = Multipart.FormData(
      Multipart.FormData.BodyPart.Strict("dial_secure_key", HttpEntity(apiKey)),
      Multipart.FormData.BodyPart.fromPath("file_perf", ContentTypes.`text/csv(UTF-8)`, csv)
    )

    Marshal(data).to[RequestEntity].flatMap { entity =>
      val request =
        HttpRequest(method = HttpMethods.POST, uri = Uri(s"$url/acquisition/upload-perf-api"), entity = entity)

      Http().singleRequest(request).flatMap {
        case HttpResponse(statusCode, _, entity, _) if statusCode.isSuccess() =>
          entity.discardBytes()
          println("Successfully sent reports:")
          Using(Source.fromFile(csv.toFile, "UTF-8"))(_.getLines().foreach(println))
          Future.successful {}
        case HttpResponse(errorCode, _, entity, _) =>
          println(s"Uploading the report in the dial has failed with status $errorCode, getting more information...")
          println("File is:")
          Using(Source.fromFile(csv.toFile, "UTF-8"))(_.getLines().foreach(println))
          println("Error is:")
          Unmarshal(entity).to[String].flatMap { response =>
            println(s"Sending ${csv.toAbsolutePath.toString} to dial failed with code $errorCode: $response")
            Future.failed(
              new IllegalArgumentException(
                s"Sending ${csv.toAbsolutePath.toString} to dial failed with code $errorCode: $response"
              )
            )
          }
      }
    }

  }

}
