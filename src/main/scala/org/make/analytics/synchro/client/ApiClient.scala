package org.make.analytics.synchro.client

import java.time.ZonedDateTime
import java.util.UUID

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{Authorization, OAuth2BearerToken, RawHeader}
import akka.http.scaladsl.unmarshalling.Unmarshal
import de.heikoseeberger.akkahttpcirce.ErrorAccumulatingCirceSupport
import io.circe.Decoder
import org.make.analytics.synchro.client.ApiClient.{AccessToken, Question}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ApiClient(implicit val actorSystem: ActorSystem) extends ErrorAccumulatingCirceSupport {
  private val config =
    actorSystem.settings.config.getConfig("analytics-synchro.make-api")

  private val url = config.getString("url")
  private val clientId = config.getString("client-id")
  private val clientSecret = config.getString("client-secret")

  private val sessionId = UUID.randomUUID().toString
  private val header: HttpHeader = RawHeader("x-make-session-id", sessionId)

  def login: Future[AccessToken] = {
    val entity = s"grant_type=client_credentials&client_id=$clientId&client_secret=$clientSecret"

    Http()
      .singleRequest(
        HttpRequest(
          method = HttpMethods.POST,
          uri = Uri(s"$url/oauth/access_token"),
          entity = HttpEntity(ContentTypes.`application/x-www-form-urlencoded`, entity),
          headers = Seq(header)
        )
      )
      .flatMap {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Unmarshal(entity).to[AccessToken]
        case _ =>
          Future.failed(new IllegalStateException("Unable to connect to make API, please check your settings"))
      }
  }

  def listOpenQuestions(token: String, day: ZonedDateTime): Future[Seq[Question]] = {
    Http()
      .singleRequest(
        HttpRequest(
          method = HttpMethods.GET,
          uri = Uri(s"$url/moderation/operations-of-questions?opentAt=${day.toString}&_end=9999"),
          headers = Seq(header, Authorization(OAuth2BearerToken(token)))
        )
      )
      .flatMap {
        case HttpResponse(StatusCodes.OK, _, entity, _) =>
          Unmarshal(entity).to[Seq[Question]]
        case _ =>
          Future.failed(new IllegalStateException("Unable to retrieve questions, please check your settings"))
      }
  }

  def listActiveQuestionSlugs(date: ZonedDateTime): Future[Seq[String]] = {
    for {
      token     <- login
      questions <- listOpenQuestions(token.token, date)
    } yield {
      questions.filter { question =>
        question.startDate.exists(_.isBefore(date)) &&
        question.endDate.exists(_.isAfter(date))
      }.map(_.slug)
    }
  }
}

object ApiClient {
  case class AccessToken(token: String)

  object AccessToken {
    implicit val decoder: Decoder[AccessToken] = Decoder.forProduct1("access_token")(AccessToken.apply)
  }

  case class Question(id: String, startDate: Option[ZonedDateTime], endDate: Option[ZonedDateTime], slug: String)

  object Question {
    implicit val decoder: Decoder[Question] = io.circe.generic.semiauto.deriveDecoder
  }

}
