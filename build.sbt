name         := "analytics-synchro"
organization := "org.make"

scalaVersion := "2.13.2"

libraryDependencies ++= Seq(
  "com.typesafe.akka"    %% "akka-http"       % "10.2.0",
  "com.typesafe.akka"    %% "akka-actor"      % "2.6.8",
  "com.typesafe.akka"    %% "akka-stream"     % "2.6.8",
  "com.typesafe"         % "config"           % "1.4.0",
  "de.heikoseeberger"    %% "akka-http-circe" % "1.33.0",
  "io.circe"             %% "circe-generic"   % "0.13.0",
  "com.github.tototoshi" %% "scala-csv"       % "1.3.6"
)

git.formattedShaVersion := git.gitHeadCommit.value.map { sha => sha.take(10) }

version in ThisBuild := {
  git.formattedShaVersion.value.get
}

mainClass in Compile := Some("org.make.analytics.synchro.Main")

addCommandAlias("checkStyle", ";scalastyle;test:scalastyle;scalafmtCheckAll;scalafmtSbtCheck")
addCommandAlias("fixStyle", ";scalafmtAll;scalafmtSbt")
