enablePlugins(UniversalPlugin)
enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

dockerBaseImage  := "azul/zulu-openjdk-centos:14"
dockerRepository := Some("nexus.prod.makeorg.tech")

Docker / daemonUser    := "analytics-synchro"
Docker / daemonUserUid := Some("300")

Docker / daemonGroup    := "apps"
Docker / daemonGroupGid := Some("2000")

packageName in Docker := "analytics-synchro"

dockerCmd := Seq("-Dfile.encoding=UTF-8")

publishLocal := {
  (Universal / packageBin).value
  (Docker / publishLocal).value
}

publish := {
  (Universal / packageBin).value
  (Docker / publish).value
}
