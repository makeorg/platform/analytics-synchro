addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager"    % "1.7.4")
addSbtPlugin("com.typesafe.sbt" % "sbt-git"                % "1.0.0")
addSbtPlugin("org.scalastyle"   %% "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("org.scalameta"    % "sbt-scalafmt"           % "2.4.3")

classpathTypes += "maven-plugin"
