# Analytics synchro - upload facebook campaigns data in the dial

This project is a batch to upload facebook reports in the dial.
It should run on a daily basis at a given time.
Since it uploads the reports for the day before, it should be run in the morning.

It will:
- List the open consultations from the API
- List all the facebook campaigns and filter the ones corresponding to open consultations
- retrieve all the insights for these campaigns
- Upload the aggregated file in the Dial

To work, it then needs:
- Credentials to connect to the API (using the client_credentials grant type)
- Credentials to use the facebook API
- Credentials to use the dial

## Running the batch

You will need a configuration file with the following structure:

```hocoon
analytics-synchro {
  facebook {
    fr {
      token = "<your facebook api token>"
      account-id = "<your facebook account id>"
    }
    de {
      token = "<your facebook api token>"
      account-id = "<your facebook account id>"
    }    
  }

  make-api {
    url = <URL to the API>"
    client-id = "<clientId for the API>"
    client-secret = "<client secret for the API>"
  }

  dial {
    url = "<base url for the dial>"
    dial-secure-key = "<Insert here you api key for the dial>"
  }
}
``` 

Then running the application can be run with docker:

`docker run -v analytics-synchro.conf:/var/run/secrets/analytics-synchro.conf nexus.prod.makeorg.tech/analytics-synchro:production-latest`

This example expects the configuration file to be called `analytics-synchro.conf` and be in the current directory. 
It then starts the production version for this batch. 

 